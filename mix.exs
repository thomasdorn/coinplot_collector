defmodule CoinplotCollector.MixProject do
  use Mix.Project

  def project do
    [
      app: :coinplot_collector,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :ethereumex, :crypto, :httpoison, :ssl],
      mod: {CoinplotCollector.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:joken, "~> 2.0"},
      {:castore, "~> 0.1.0"},
      {:mint, "~> 1.0"},
      {:tesla, "~> 1.3.0"},
      {:jason, "~> 1.1"},
      {:jsonrpc2, "~> 1.0"},
      {:mongodb, ">= 0.0.0"},
      {:poolboy, "~> 1.5"},
      {:cowboy, "~> 2.5"},
      {:plug, "~> 1.4"},
      {:plug_cowboy, "~> 2.1.2"},
      {:yajwt, "~> 1.0"},
      {:credo, "~> 0.10", except: :prod, runtime: false},
      {:poison, "~> 3.1"},
      {:hackney, "~> 1.15"},
      {:ethereumex, "~> 0.2.0"},
      {:memoize, "~> 1.3"},
      {:distillery, "~> 2.0"},
      {:httpoison, "~> 1.0"},
    ]
  end
end
