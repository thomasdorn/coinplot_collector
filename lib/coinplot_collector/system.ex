defmodule CoinplotCollector.System do
  def start_link do
    import Supervisor.Spec
    Supervisor.start_link(
      [
        worker(Mongo, [[name: :mongo, pool_size: 7, url: "mongodb://172.16.1.12:27017/coinplotCollector"]]),
        CoinplotCollector.Looper,
        CoinplotCollector.Web,
        CoinplotCollector.Controllers.Ethereum,
        CoinplotCollector.Controllers.User,
        Registry.child_spec(
          keys: :duplicate,
          name: :process_registry
        )
      ],
      strategy: :one_for_one, name: CoinplotCollector.Supervisor
    )
  end
end
