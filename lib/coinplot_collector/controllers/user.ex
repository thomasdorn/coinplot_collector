defmodule CoinplotCollector.Controllers.User do
  alias CoinplotCollector.Controllers.User.Worker, as: Worker
  @pool_size 5
  def child_spec(_) do
    :poolboy.child_spec(
      __MODULE__,
      [
        name: {:local, __MODULE__},
        worker_module: Worker,
        size: @pool_size,
        max_overflow: 2
      ],
      []
    )
  end

  def create(username, password) do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid ->
        Worker.create_new_user(worker_pid, username, password)
      end
    )
  end

  def login(username, password) do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid ->
         {:ok, result} = Worker.login_user(worker_pid, username, password)
      end
    )
  end
end


defmodule CoinplotCollector.Controllers.User.Worker do
  alias CoinplotCollector.Auth.Jwt, as: Jwt

  use GenServer
  def start_link(name) do
    GenServer.start_link(
      __MODULE__,
      []
    )
  end

  @impl GenServer
  def init(_name) do
    {:ok, []}
  end

  def create_new_user(pid, username, password) do
    GenServer.call(pid, {:create_new_user, username, password})
  end

  def login_user(pid, username, password) do
    GenServer.call(pid, {:login_user, username, password})
  end

  @impl GenServer
  def handle_call({:create_new_user, username, password}, _, state) do
    IO.puts("interacted with user genserver::create_new_user")
    userQuery = %{
      "username" => username
    }
    cursor = Mongo.find(:mongo, "users", userQuery, sort: %{count: -1}) |> Enum.to_list
    case_result = case Enum.count(cursor) do
      0 ->
        hawkId = :crypto.hash(:sha, username) |> Base.encode64
        hawkKey = :crypto.hash(:sha, Randomizer.randomizer(25, :numeric)) |> Base.encode64
        passwordMd5 = :crypto.hash(:md5, password) |> Base.encode64
        userDoc = %{
          "username" => username,
          "password" => passwordMd5,
          "hawkId" => hawkId,
          "hawkKey" => hawkKey
        }
        userInfoDoc = %{
          "username" => username,
          "transactions" => []
        }
        Mongo.insert_one(:mongo, "userInfo", userInfoDoc)
        {:ok, userDoc}
      _ ->
        {:ok,
          %{
            "status" => "failed",
            "message" => "user found"
          }
        }
    end
    {:reply, case_result, [:create_new_user]}
  end

  @impl GenServer
  def handle_call({:login_user, username, password}, _, state) do
    IO.puts("interacted with user genserver::login_user")
    passwordMd5 = :crypto.hash(:md5, password) |> Base.encode64
    userQuery = %{
      "username" => username,
      "password" => passwordMd5
    }
    cursor = Mongo.find(:mongo, "users", userQuery, sort: %{count: -1}) |> Enum.to_list
    case_result = case Enum.count(cursor) do
      1 ->
        IO.puts "login successful"
        userDoc = List.first(cursor)
        {:ok,
          %{
            "error" => false,
            "data" => %{
              "hawkId" => userDoc["hawkId"],
              "hawkKey" => userDoc["hawkKey"],
              "token" => Jwt.generate()
            }
          }
        }
      _ ->
        {:ok,
          %{
            "error" => true,
            "status" => "failed",
            "message" => "no user or bad password"
          }
        }
    end
    {:reply, case_result, [:login_user]}
  end

end
