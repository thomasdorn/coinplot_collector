defmodule CoinplotCollector.Controllers.Ethereum do
  alias CoinplotCollector.Controllers.Ethereum.Worker, as: Worker
  @pool_size 5
  def child_spec(_) do
    :poolboy.child_spec(
      __MODULE__,
      [
        name: {:local, __MODULE__},
        worker_module: Worker,
        size: @pool_size,
        max_overflow: 2
      ],
      []
    )
  end
  def new_account(password) do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid ->
         {:ok, result} = Worker.access_new_account(worker_pid, password)
      end
    )
  end
  def list_accounts do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid ->
         {:ok, result} = Worker.access_list_accounts(worker_pid)
      end
    )
  end

  def send_ethereum_transaction do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid ->
        {:ok, result} = Worker.access_list_accounts(worker_pid)
      end
    )
  end

  def run_command(cmd1, cmd2, list) do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid ->
        Worker.access_run_command(worker_pid, cmd1, cmd2, list)
      end
    )
  end
end

defmodule CoinplotCollector.Controllers.Ethereum.Worker do
  alias JSONRPC2.Clients.HTTP
  @url Application.fetch_env!(:coinplot_collector, :geth_url)
  use GenServer

  def start_link(_name) do
    GenServer.start_link(
      __MODULE__,
      []
    )
  end

  defp run_command(command1, command2, opts \\ []) do
    HTTP.call(@url, command1 <> "_" <> command2, opts)
  end

  def access_list_accounts(pid) do
    GenServer.call(pid, {:list_accounts})
  end

  @spec access_new_account(atom | pid | {atom, any} | {:via, atom, any}, any) :: any
  def access_new_account(pid, password) do
    GenServer.call(pid, {:new_account, password})
  end

  @impl GenServer
  def init(_name) do
    {:ok, []}
  end

  @impl GenServer
  def handle_call({:run_command, cmd1, cmd2, list}, _, state) do
    IO.puts("interacted with eth genserver")
    new_state = Enum.concat(state , {cmd1, cmd2, list} ) |> IO.inspect
    {:ok, result} = run_command(cmd1, cmd2, list)
    {:reply, result, new_state}
  end

  @impl GenServer
  def handle_call({:list_accounts}, _, state) do
    IO.puts("interacted with eth genserver")
    result = run_command("personal", "listAccounts")
    {:reply, result, state}
  end

  @impl GenServer
  def handle_call({:new_account, key}, _, state) do
    IO.puts("interacted with eth genserver")
    result = run_command("personal", "newAccount", [key])
    {:reply, result, []}
  end
end
