defmodule CoinplotCollector.Looper do
  use Task
  use Tesla

  plug Tesla.Middleware.BaseUrl, "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest"
  plug Tesla.Middleware.JSON

  @ethereum "ethereum"
  @bitcoin "bitcoin"
  @zcash "zcash"

  def start_link(_args) do
    Task.start_link(&loop/0)
  end

#  def child_spec(_) do
#    Supervisor.child_spec(
#      CoinplotCollector.Looper,
#      id: __MODULE__,
#      start: {__MODULE__, :start_link, []}
#    )
#  end

#  defp write_to_mongo(coinname, document) do
#    {:ok, count} = Mongo.count_documents(:mongo, coinname, %{})
#    document = Map.put(document, "count", count+1)
#    Mongo.insert_one(:mongo, coinname, document)
#
#    IO.puts("Document written to database " <> coinname <> " " <> :erlang.float_to_binary(document["price"], decimals: 2))
#  end

#  defp compute_24_hour_stats(namein) do
#    Task.start_link(fn ->
#      datalist = Mongo.find(:mongo, namein, %{}, limit: 288) |> Enum.to_list()
#      average = Enum.map(datalist, fn item -> item["price"] end)
#                |> Enum.reduce(fn (num, acc) -> num + acc end)
#                |> (&(&1/length(datalist))).()
#      IO.puts("The 24 hour price average is " <> :erlang.float_to_binary(average, decimals: 2) <> " for " <> namein)
#      {:ok, count} = Mongo.count_documents(:mongo, namein <> "_24h_average", %{})
#      doc = %{"24h" => average, "count" => count + 1}
#      Mongo.insert_one(:mongo, namein <> "_24h_average", doc)
#    end)
#  end

  defp loop() do
#     Task.start_link(
#        fn ->
#          {:ok, response} = get("v2/ticker/1/")
##           response.body
##            |> (&Map.get(&1, "data")).()
##            |> (&Map.get(&1, "quotes")).()
##            |> (&Map.get(&1, "USD")).()
##            |> (&write_to_mongo(@bitcoin, &1)).()
##           compute_24_hour_stats(@bitcoin)
#         end
#       )
##    Task.start_link(
#      fn ->
#        {:ok, response} = get("v2/ticker/1/")
#        response.body
#        |> (&Map.get(&1, "data")).()
#        |> (&Map.get(&1, "quotes")).()
#        |> (&Map.get(&1, "USD")).()
#        |> (&write_to_mongo(@bitcoin, &1)).()
#        compute_24_hour_stats(@bitcoin)
#      end
#    )
#    Task.start_link(
#    fn ->
#        {:ok, response} = get("v2/ticker/1027/")
#        response.body
#        |> (&Map.get(&1, "data")).()
#        |> (&Map.get(&1, "quotes")).()
#        |> (&Map.get(&1, "USD")).()
#        |> (&write_to_mongo(@ethereum, &1)).()
#        compute_24_hour_stats(@ethereum)
#    end
#    )
#    Task.start_link(
#      fn ->
#        {:ok, response} = get("v2/ticker/1437/")
#        response.body
#        |> (&Map.get(&1, "data")).()
#        |> (&Map.get(&1, "quotes")).()
#        |> (&Map.get(&1, "USD")).()
#        |> (&write_to_mongo(@zcash, &1)).()
#        compute_24_hour_stats(@zcash)
#      end
#    )
    collect_metrics() |> IO.inspect
    Process.sleep(:timer.seconds(5))
    loop()
  end

  defp collect_metrics() do
    [
      memory_usage: :erlang.memory(:total),
      process_count: :erlang.system_info(:process_count)
    ]
  end
end
