defmodule CoinplotCollector.Service.Bitcoin do
  use Tesla

  @proxy_url Application.fetch_env!(:coinplot_collector, :bitcoin_proxy_url)
  @smartbit Application.fetch_env!(:coinplot_collector, :smartbit_url)

  plug Tesla.Middleware.BaseUrl, @smartbit
  plug Tesla.Middleware.JSON

  def info() do
    body = %{
      "id" => "curltest",
      "method" => "getblockchaininfo",
      "params" => []
    }
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} = HTTPoison.post(@proxy_url, Poison.encode!(body), [{"Content-Type", "application/json"}])
    {:ok, Poison.decode!(body)}
  end

  def new_account() do
    body = %{
      "id" => "curltest",
      "method" => "getnewaddress",
      "params" => []
    }
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} = HTTPoison.post(@proxy_url, Poison.encode!(body), [{"Content-Type", "application/json"}])
    {:ok, body}
  end

  def get_address_info(address) do
    body = %{
      "id" => "curltest",
      "method" => "getaddressinfo",
      "params" => [address]
    }
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} = HTTPoison.post(@proxy_url, Poison.encode!(body), [{"Content-Type", "application/json"}])
    {:ok, Poison.decode!(body)}
  end



  def get_balance_from_smart_bit(address) do
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} = HTTPoison.get("https://api.smartbit.com.au/v1/blockchain/address/" <> address)
    {:ok, Poison.decode!(body)}
  end



  def get_balance(address) do
    body = %{
      "id" => "curltest",
      "method" => "getbalance",
      "params" => ["*", 6]
    }
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} = HTTPoison.post(@proxy_url, Poison.encode!(body), [{"Content-Type", "application/json"}])
    {:ok, body}
  end
end
