defmodule CoinplotCollector.Service.Transaction do
  alias CoinplotCollector.Service.Bitcoin, as: Bitcoin
  alias CoinplotCollector.Controllers.Ethereum, as: Ethereum

  defp send_ethereum(send_to_address) do
    IO.puts "send ethereum out"
  end

  def btc_eth(address, send_to_address, amount) do
    Process.sleep(:timer.seconds(2))

    case Bitcoin.get_balance_from_smart_bit(address) do
      {:ok, %{"address" => %{"confirmed" => %{"balance"=> balance}}}} ->
        balance_float = String.to_float(balance)
        #handle the case of a different amount
        if  balance_float == amount and amount > 0 do
          IO.puts "balance confirmed"
          Ethereum.send_ethereum_transaction
        end

        if balance_float != amount and amount > 0 do
          IO.puts "balance confirmed"

          different_amount(address, send_to_address, amount)
        end
        btc_eth(address, send_to_address, amount)
      {:error, error} -> on_error(address, send_to_address, amount)

    end

  end

  def create(username, coinplot_transaction) do

  end

  defp different_amount(address, send_to_address, amount) do

  end

  defp on_error(address, send_to_address, amount) do
    #check if deposited a different amount
    #check if smartbit is working
  end
end
