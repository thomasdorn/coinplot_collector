defmodule CoinplotCollector.Auth.Hawk do
  def process_hawk(conn) do
    {:ok, datetime} = DateTime.now("Etc/UTC")
    attributes = parse_header(conn)
    request = parse_request(conn)
    hawk_id = attributes["hawkId"]
    mac = attributes["mac"]
    nonce = attributes["nonce"]
    method = request["method"]
    token = Map.fetch!(conn.params, "token")
    url = request["url"] <> "?token=" <> token
    host = request["host"]
    port = request["port"]
    userQuery = %{ "hawkId" => hawk_id }
    cursor = Mongo.find(:mongo, "users", userQuery, sort: %{count: -1}) |> Enum.to_list
    IO.puts "resource requested " <> request["url"]
    case Enum.count(cursor) do
      1 ->
        time_now = DateTime.to_unix(datetime)
        ts = attributes["ts"]
        difference = time_now / ts |> IO.inspect
        if time_now == ts do
          IO.puts "time stamp good " <> Float.to_string(difference, decimals: 20)
          ts_string = Integer.to_string(ts)
          normalized_string = nonce <> ts_string <> url
          user = List.first(cursor)
          {:ok, bytes_key} = Base.decode64(user["hawkKey"])
          computed_mac = :crypto.hmac(:sha256, bytes_key, normalized_string) |> Base.encode64
          if computed_mac == mac do
            IO.puts "Mac Good"
            true
          else
            IO.puts "Bad Mac"
            false
          end
        else
          IO.puts "Time Stamp Stale"
          false
        end
      _ ->
        IO.puts "User Error"
        false
    end
  end

  defp parse_header(conn) do
    auth = Plug.Conn.get_req_header(conn, "authorization")
    auth_first_hawk_id = List.first(auth)
                         |> String.split(",")
                         |> Enum.at(0)
                         |> String.replace("\"","")
    auth_first_ts = List.first(auth)
                    |> String.split(",")
                    |> Enum.at(1)
                    |> String.replace("\"","")
                    |> String.replace(" ","")
    auth_first_nonce = List.first(auth)
                       |> String.split(",")
                       |> Enum.at(2)
                       |> String.replace("\"","")
                       |> String.replace(" ","")
    auth_first_mac = List.first(auth)
                     |> String.split(",")
                     |> Enum.at(3)
                     |> String.replace("\"","")
                     |> String.replace(" ","")
    hawk_id = String.split(auth_first_hawk_id, "=") |> Enum.at(1)
    hawk_id_plus = hawk_id <> "="
    ts = String.split(auth_first_ts, "=") |> Enum.at(1) |> String.to_integer()
    nonce = String.split(auth_first_nonce, "=") |> Enum.at(1)
    mac_k = String.split(auth_first_mac, "=") |> Enum.at(1)
    mac = mac_k <> "="
    %{
      "ts" => ts,
      "hawkId" => hawk_id_plus,
      "nonce" => nonce,
      "mac" => mac
    }
  end

  defp parse_request(conn) do
    method = conn.method
    headers = conn.req_headers
    url_step = Enum.filter(headers,
        fn x ->
          list = Tuple.to_list(x)
          if Enum.member?(list, "origin") do
            x
          end
          if Enum.member?(list, "referer") do
            x |>
            Tuple.to_list |> (&Enum.at(&1, 1)).() |> String.split("/") |> (&Enum.at(&1, 2)).()
          end
        end)
    url_step2 = List.first(url_step) |> Tuple.to_list |> (&Enum.at(&1, 1)).()
    auth_step = Enum.filter(headers,
      fn x ->
        list = Tuple.to_list(x)
        if Enum.member?(list, "authorization") do
          x
        end
      end)
    auth = (List.first(auth_step) |> Tuple.to_list |> (&Enum.at(&1, 1)).())
    if url_step2 =~ "https://" do
      url_step3 = String.replace(url_step2, "https://", "")
      url_step4 = String.split(url_step3, ":") |> List.first
      test_port =  String.split(url_step3, ":") |> (&Enum.at(&1, 1)).()
      port = if test_port == nil do
          "443"
        else
          test_port
      end
      %{
        "method" => method,
        "url" => conn.request_path,
        "host" => url_step4,
        "port" => port,
        "authorization" => auth,
        "contentType" => "application/json"
      }
    else
      url_step3 = String.replace(url_step2, "http://", "")
      url_step4 = String.split(url_step3, ":") |> List.first
      port = String.split(url_step3, ":") |> (&Enum.at(&1, 1)).()
      %{
        "method" => method,
        "url" => conn.request_path,
        "host" => url_step4,
        "port" => port,
        "authorization" => auth,
        "contentType" => "application/json"
      }
    end
  end
end
