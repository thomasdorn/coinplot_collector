defmodule CoinplotCollector.Web do

  alias CoinplotCollector.Auth.Jwt, as: Jwt
  alias CoinplotCollector.Auth.Hawk, as: Hawk
  alias CoinplotCollector.Controllers.User, as: User
  alias CoinplotCollector.Controllers.Ethereum, as: Ethereum
  alias CoinplotCollector.Service.Bitcoin, as: Bitcoin

  use Plug.Router
  use Memoize
  use Task

  plug :match

  plug(
    Plug.Parsers,
    parsers: [:json, :urlencoded],
    pass: ["application/json"],
    json_decoder: Poison
  )

  def child_spec(_arg) do
    Plug.Adapters.Cowboy.child_spec(
      scheme: :http,
      options: [
        port: 5454,
        dispatch: dispatch
      ],
      plug: __MODULE__
    )

  end

  plug :dispatch

  defp dispatch do
    [
      {:_,
        [
          {"/ws/[...]", CoinplotCollector.Socket, []},
          {:_, Plug.Cowboy.Handler, {__MODULE__, []}}
        ]
      }
    ]
  end

  get "api/test" do
    conn
    |> Plug.Conn.put_resp_content_type("application/json")
    |> Plug.Conn.send_resp(200, Poison.encode!("result"))
  end

  match "_" do
    conn
    |> Plug.Conn.put_resp_content_type("application/json")
    |> Plug.Conn.send_resp(200, Poison.encode!(%{
      "error" => true,
      "status" => "failed",
      "message" => "no route"
    }))
  end

  defp auth_response(conn, callback) do
    hawk_valid = Hawk.process_hawk(conn)
    token_valid = Jwt.verify_token(conn)
    if hawk_valid and token_valid do
      {:ok, response} = callback.(conn)
      conn
      |> Plug.Conn.put_resp_content_type("application/json")
      |> Plug.Conn.send_resp(200, Poison.encode!(%{
        "error" => false,
        "status" => "success",
        "message" => "got data, passed authentication",
        "data" => response
      }))
    else
      conn
      |> Plug.Conn.put_resp_content_type("application/json")
      |> Plug.Conn.send_resp(401, Poison.encode!(%{
        "error" => true,
        "status" => "failed",
        "message" => "Bad Auth",
        "data" => %{}
      }))
    end
  end

  ## POST REQUEST
  post "api/ethereum/new_account" do
    conn |> auth_response(fn conn ->
      body = conn.params
      password = body["password"]
      Ethereum.new_account(password)
    end)
  end

  get "api/auth/pre_request" do
    IO.puts "pre pequest"
    {:ok, datetime} = DateTime.now("Etc/UTC")
    time_unix = DateTime.to_unix(datetime)
    nonce = :crypto.hash(:sha, Randomizer.randomizer(25, :numeric)) |> Base.encode64 |> String.slice(0..5)
    conn
    |> Plug.Conn.put_resp_content_type("application/json")
    |> Plug.Conn.send_resp(200, Poison.encode!(%{
      "error" => false,
      "status" => "success",
      "message" => "pre auth request",
      "data" => %{
        "nonce" => nonce,
        "serverTime" => time_unix
      }
    }))
  end

  get "api/ethereum/:action" do
    case action do
      "list_accounts" -> conn |> auth_response(fn conn -> Ethereum.list_accounts end)
      "info" -> conn |> auth_response(fn conn -> Ethereumex.HttpClient.web3_client_version end)
    end
  end

  get "api/transaction/:action" do
    case action do
      "btc_eth" -> conn |> auth_response(fn conn ->
        body = conn.params
        username = body["username"]
        coinplot_address = body["coinplotAddress"]
        send_address = body["sendAddress"]
        transaction_id  = :crypto.hash(:sha, Randomizer.randomizer(25, :numeric)) |> Base.encode64
#        old_user_doc = User.info(username)
#        transactions = old_user_doc["transactions"]
#        new_transactions = [transactions | transaction_id]
#        new_user_doc = %{
#          "username" => username,
#          "transactions" => new_transactions
#        }
#        User.update(username, new_user_doc)
        coinplot_transaction = %{
          "username" => username,
          "type" => "btc_eth",
          "coinplot_address" => coinplot_address,
          "send_address" => send_address,
          "transaction_id" => transaction_id,
          "monitor" => true,
          "status" => "started"
        }
        Transaction.create(username, coinplot_transaction)


        {:ok, %{
            "transaction_id" => transaction_id
          }}
      end)
    end
  end

  get "api/bitcoin/:action" do
    case action do
      "new_account" -> conn |> auth_response(fn conn -> Bitcoin.new_account end)
      "list_accounts" -> conn |> auth_response(fn conn -> Ethereum.list_accounts end)
      "info" -> conn |> auth_response(fn conn -> Bitcoin.info end)
    end
  end

  defmemo get_price(coinname), expires_in: 300 do
    cursor = Mongo.find(:mongo, "#{coinname}", %{}, limit: 1, sort: %{count: -1}) |> Enum.to_list
    first = List.first(cursor)
  end

  get "api/price/:coinname" do
    result = get_price(coinname)
    conn
    |> Plug.Conn.put_resp_content_type("application/json")
    |> Plug.Conn.send_resp(200, Poison.encode!(result))
  end

  ## GET REQUESTS

  get "api/user/:action" do
    case action do
      "user_info" -> conn |> auth_response(fn conn -> User.info end)
    end
  end

  post "api/user/:action" do
    case action do
      "create" ->
#        conn |> User.create |> unauth_response
        IO.puts "ran user create"
        body = conn.params
        username = body["username"]
        password = body["password"]
        response = User.create(username, password)
        conn
        |> Plug.Conn.put_resp_content_type("application/json")
        |> Plug.Conn.send_resp(200, Poison.encode!(response))
      "login" ->
        IO.puts "ran user login"
        body = conn.params
        password = body["password"]
        username = body["username"]
        {:ok, response} = User.login(username, password)
        conn
        |> Plug.Conn.put_resp_content_type("application/json")
        |> Plug.Conn.send_resp(200, Poison.encode!(response))
      "hawk" ->
        conn |> auth_response(fn conn ->
          body = conn.params
          userQuery = %{ "username" => body["username"] }
          cursor = Mongo.find(:mongo, "users", userQuery, sort: %{count: -1}) |> Enum.to_list |> List.first
          hawk_key = cursor["hawkKey"]
          {:ok,
              %{"hawkKey" => cursor["hawkKey"]}
            }
        end)
    end
  end
end
