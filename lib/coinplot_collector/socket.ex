defmodule CoinplotCollector.Socket do
  @behaviour :cowboy_websocket

  @impl GenServer
  def init(request, _state) do
    state = %{registry_key: request.path}
    {:cowboy_websocket, request, state}
  end

  def websocket_init(state) do
    Registry.register(:process_registry, state.registry_key, {})
    {:ok, state}
  end

  def websocket_handle({:text, json}, state) do
    payload = Jason.decode!(json)
    message = payload["data"]["message"]
    IO.inspect(message)
    IO.inspect(state)
    Registry.dispatch(:process_registry, state.registry_key, fn(entries) ->
      for {pid, _} <- entries do
        if pid != self() do
          IO.inspect(message)
          Process.send(pid, message, [])
        end
      end
    end)

    {:reply, {:text, message}, state}
  end

  def websocket_info(info, state) do
    IO.inspect(info)
    {:reply, {:text, info}, state}
  end

  @impl GenServer
  def handle_info(:timeout, {name, state}) do
    IO.puts("Stopping socket server for #{name}")
    {:stop, :normal, {name, state}}
  end

end
